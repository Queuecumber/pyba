{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "w4wpwxli1ppB"
      },
      "source": [
        "# Stream Formats\n",
        "\n",
        "Now that we can get packets from the correct stream in the video file, we can start to parse them. This leads us to \"stream formats\" which are the binary representation of the video stream itself. For H264, this is a way to encode the list of NAL units such that NAL unit boundaries can be easily identified. If we simply concatenated the bits of each of the NALUs and dumped the result to disk, we wouldn't be able to easily split the result up into the individual NALUs in order to decode the stream. The stream formats define a standardized way identify the NALU boundaries in a bitstream. As usual, there are multiple way to do this. We will show two common formats: **Annex B** and **AVCC**, each of which has different advantages and disadvantages."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "8cCkGJPm2my6"
      },
      "source": [
        "## Annex B\n",
        "\n",
        "The Annex B format is defined in Annex B of the H264 standard (page 307 of the version I'm using from 06/2019). The format is extremely simple, each NALU starts with either the 24 bit sequence `0x000001` or the 32 bit sequence `0x00000001`. When parsing the stream, you can simply read through in 32bit chucks looking for these sequences. When you find one, you have hit a NALU. Then you continue until you find one again, this is the start of the next NALU so you can safely parse what you've just read as a complete NALU. \n",
        "\n",
        "The advantage of this format is that it is extremely simple. The disadvantage is that it is hard to seek to individual frames. For example if you wanted to seek to frame 10, you'd need to read the entire file looking for 10 NALU boundaries even though you don't care about those first 9 frames. You might be wondering why the start code can't appear in the middle of the binary data of the NALU itself. This is another disadvantage of Annex B: it could, so after encoding, any bytes inside the NALU that match the start code are replaced by something called \"emulation prevention\" bytes which can be identified and removed by the decoder. We'll talk about this more later. Because it is a mandatory part of the standard, emulation prevention bytes are inserted regardless of the stream format."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "wyscTC7Z48iU"
      },
      "source": [
        "First, let's do some imports"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 12,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "-ic6NrvF4rC8",
        "outputId": "930eb7a3-7000-489e-ed98-34cacb60c055"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "Requirement already satisfied: bitstring in /home/max/pyba/.venv/lib/python3.9/site-packages (3.1.7)\n"
          ]
        }
      ],
      "source": [
        "!pip install bitstring\n",
        "import bitstring"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "EeAzS_r45oZ7"
      },
      "source": [
        "Next we can use bitstring to read the file and look for the start code. "
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 13,
      "metadata": {
        "id": "dronW9ca7SFz"
      },
      "outputs": [],
      "source": [
        "stream_bits = bitstring.ConstBitStream(filename=\"../assets/sample.h264\")\n",
        "while stream_bits.peek(24) != \"0x000001\" and stream_bits.peek(32) != \"0x00000001\":\n",
        "    stream_bits.read(8)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "2qFrijh-78bM"
      },
      "source": [
        "Once we've found the start code, we need to clean things up a little. The start code could have been 24 or 32 bits, if it was 32bits, we'll discard 8 bits to force it to be 24bits. This will make things simpler for us."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 23,
      "metadata": {
        "id": "EaVNofXr8Ktr"
      },
      "outputs": [],
      "source": [
        "if stream_bits.peek(24) != \"0x000001\":\n",
        "    stream_bits.read(8)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "EbI8C-uI8MgZ"
      },
      "source": [
        "Next we can make a new place to hold the NALU bits and read the 24bit start code into it"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 24,
      "metadata": {
        "id": "nOr_XF6J8RIP"
      },
      "outputs": [],
      "source": [
        "nalu_bits = bitstring.BitStream()\n",
        "nalu_bits.append(stream_bits.read(24))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "ZKWLcPcX8UPN"
      },
      "source": [
        "And finally, we read through the stream one byte at a time looking for the start code again, storing the results in our NALU buffer."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 25,
      "metadata": {
        "id": "tXIOMnGH8eLu"
      },
      "outputs": [],
      "source": [
        "while stream_bits.peek(24) != \"0x000001\" and stream_bits.peek(32) != \"0x00000001\":\n",
        "    nalu_bits.append(stream_bits.read(8))"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "BZ8S0bvw8g-S"
      },
      "source": [
        "This gives us a completed NALU, let's read the size since we don't know how to parse it yet"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 26,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "HGRsotSJ8uoN",
        "outputId": "6d2a15a0-3900-4405-d701-64b5eaf1e18c"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "NALU Size: 248\n"
          ]
        }
      ],
      "source": [
        "print(f'NALU Size: {len(nalu_bits)}')"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "dETxk4hn-b4S"
      },
      "source": [
        "## AVCC\n",
        "\n",
        "The AVCC format is slightly more complex but is the primary format used in containers (like mp4). In this format, each NALU is preceeded not by a start code but by the size in bytes of the NALU. Unfortunately, the size could be stored in 1, 2, or 4 bytes, so we need to parse a header, called the AVCC Sequence Header, in order to find out how many bytes to read before we get to actual NALU data, adding complexity. Note that a single byte means the maximum NALU size is 255 bytes and 2 bytes is ~64kB, which are generally too small, so 4 bytes is the most common. This header also has a bunch of other important things in it which would normally be stored in NALUs. \n",
        "\n",
        "The obvious advantage to this format is that it's easy to seek (and that's why its used in containers). A decoder that wants to seek to the 10th frame can simply parse the header, then read those (1, 2, 4) bytes from each NALU, then skip that many bytes in the stream, then read the next size and skip again until it gets to the 10th frame. \n",
        "\n",
        "Since we're dealing with containes again, we'll use PyAV. Let's import it."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 27,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "v47TCPhyAQgu",
        "outputId": "ebc175f3-c846-4263-da45-94d6cc7bf81b"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "Requirement already satisfied: av in /home/max/pyba/.venv/lib/python3.9/site-packages (8.0.3)\n"
          ]
        }
      ],
      "source": [
        "!pip install av\n",
        "import av"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "Y6_2u2qrAgNK"
      },
      "source": [
        "Next we can open the video file for reading"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 28,
      "metadata": {
        "id": "_VlWy1xpAt_X"
      },
      "outputs": [],
      "source": [
        "container = av.open('../assets/sample.mp4')"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "yumLbsIsAwgP"
      },
      "source": [
        "Before doing anything else, let's parse the AVCC Sequence Header. The header looks like this:\n",
        "\n",
        "```\n",
        "version: 8 bits uint (always 0x01)\n",
        "avc_profile: 8 bits uint\n",
        "avc_compatibility: 8 bits uint\n",
        "avc_level: 8 bits uint\n",
        "reserved: 6 bits\n",
        "nalu_size_bytes_minus_one: 2 bits uint\n",
        "reserved: 3 bits\n",
        "SPS and PPS data (more on this later)\n",
        "```\n",
        "\n",
        "The important thing for what we're doing is `nalu_size_bytes_minus_one` which is one less than the number of bytes storing the NALU size (the minus one makes it fit in 2 bits since it can either be 0 (`b00`), 1 (`b01`), or 3 (`b11`)). SPS and PPS data are Sequence Parameter Sets and Picture Parameter Sets, we'll discuss them later.\n",
        "\n",
        "Let's parse this structure, we can get it from the `extradata` parameter of the PyAV stream"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 29,
      "metadata": {
        "id": "aABfajRFPrAy"
      },
      "outputs": [],
      "source": [
        "stream_extradata = container.streams.video[0].extradata\n",
        "sh_bits = bitstring.ConstBitStream(stream_extradata)\n",
        "\n",
        "version = sh_bits.read(8)\n",
        "avc_profile = sh_bits.read(\"uint:8\")\n",
        "avc_compatibility = sh_bits.read(\"uint:8\")\n",
        "avc_level = sh_bits.read(\"uint:8\")\n",
        "\n",
        "sh_bits.read(6)\n",
        "\n",
        "nalu_size_bytes_minus_one = sh_bits.read(\"uint:2\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "W1Y9bNwPQV0t"
      },
      "source": [
        "and lets print out what we parsed"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 30,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "IO4Ye3okQVv6",
        "outputId": "6b42a616-19f5-4f40-c461-f38e67f692e6"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "AVCC Version 0x01\n",
            "Profile 100\n",
            "Compatibility 0\n",
            "Level 31\n",
            "NALU Size Bytes 4\n"
          ]
        }
      ],
      "source": [
        "print(f'AVCC Version {version}')\n",
        "print(f'Profile {avc_profile}')\n",
        "print(f'Compatibility {avc_compatibility}')\n",
        "print(f'Level {avc_level}')\n",
        "print(f'NALU Size Bytes {nalu_size_bytes_minus_one + 1}')"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "9BdD5dyFUKuk"
      },
      "source": [
        "Profile 100 means \"High\" profile, and level 31 means level 3.1 (don't worry about what that means for now). Also we can see that the NALU size field is 4 bytes wide. With this information in hand we can parse NALUs, we'll get the packets with PyAV. Since we don't really know what NALUs are yet we'll just compare the size we read using the sequence header information to the size of the packet the PyAV read."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 31,
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "id": "YyMbG09oUxDJ",
        "outputId": "00f8c76d-9371-4de0-d5d0-29500bd7b80d"
      },
      "outputs": [
        {
          "name": "stdout",
          "output_type": "stream",
          "text": [
            "NALU size from header: 787, PyAV packet size: 75637\n",
            "NALU size from header: 4374, PyAV packet size: 4378\n",
            "NALU size from header: 380, PyAV packet size: 384\n",
            "NALU size from header: 80, PyAV packet size: 84\n",
            "NALU size from header: 164, PyAV packet size: 168\n",
            "NALU size from header: 287, PyAV packet size: 291\n",
            "NALU size from header: 331, PyAV packet size: 335\n",
            "NALU size from header: 7658, PyAV packet size: 7662\n",
            "NALU size from header: 901, PyAV packet size: 905\n",
            "NALU size from header: 254, PyAV packet size: 258\n",
            "NALU size from header: 444, PyAV packet size: 448\n",
            "NALU size from header: 461, PyAV packet size: 465\n",
            "NALU size from header: 453, PyAV packet size: 457\n",
            "NALU size from header: 501, PyAV packet size: 505\n",
            "NALU size from header: 305, PyAV packet size: 309\n",
            "NALU size from header: 9077, PyAV packet size: 9081\n",
            "NALU size from header: 875, PyAV packet size: 879\n",
            "NALU size from header: 351, PyAV packet size: 355\n",
            "NALU size from header: 409, PyAV packet size: 413\n",
            "NALU size from header: 405, PyAV packet size: 409\n",
            "NALU size from header: 262, PyAV packet size: 266\n",
            "NALU size from header: 10089, PyAV packet size: 10093\n",
            "NALU size from header: 609, PyAV packet size: 613\n",
            "NALU size from header: 302, PyAV packet size: 306\n",
            "NALU size from header: 380, PyAV packet size: 384\n"
          ]
        }
      ],
      "source": [
        "for packet in container.demux(video=0):\n",
        "    if packet.size > 0:\n",
        "        nalu_bits = bitstring.ConstBitStream(packet.to_bytes())\n",
        "        nalu_size_from_header = nalu_bits.read((nalu_size_bytes_minus_one + 1) * 8).uint\n",
        "\n",
        "        print(f\"NALU size from header: {nalu_size_from_header}, PyAV packet size: {packet.size}\")"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "Z5SG3tc-WIsX"
      },
      "source": [
        "Note that the packet size is exactly 4 bytes larger than the size we read. This is because the size we read is the size of the NALU itself and the packet includes the NALU plus the 4 byte size data."
      ]
    }
  ],
  "metadata": {
    "colab": {
      "collapsed_sections": [],
      "name": "streamformats.ipynb",
      "provenance": []
    },
    "interpreter": {
      "hash": "08b8dbf0351f09da8bb1c97bac8fc072cff8fcf4f5cb654fd2ca1820e9c29a93"
    },
    "kernelspec": {
      "display_name": "Python 3.9.5 64-bit ('.venv')",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.5"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 0
}